#include <memory>
#include <tuple>
#include <LogIt.h>
#include <LogLevels.h>
#include <CCanAccess.h>
#include <CanLibLoader.h>

typedef std::shared_ptr<CanModule::CanLibLoader> CanLibLoaderPtr;
typedef std::tuple<std::string/*param1*/, std::string/*param2*/> CanModuleParams;

class CanModuleConnection
{
public:
  CanModuleConnection(const std::string& name, CanLibLoaderPtr implementationLib, const CanModuleParams& params)
  :m_implementation(implementationLib), m_busAccess(implementationLib->openCanBus(std::get<0>(params), std::get<1>(params)))
  {
    LOG(Log::INF) << __FUNCTION__ << " created! got bus access ["<<std::hex<<m_busAccess<<"]";
  };

  virtual ~CanModuleConnection()
  {
    LOG(Log::INF) << __FUNCTION__ << "+ closing bus ["<<std::hex<<m_busAccess<<"]...";
    m_implementation->closeCanBus(m_busAccess);
    LOG(Log::INF) << __FUNCTION__ << "- closed bus";
  };

private:
  CanLibLoaderPtr m_implementation;
  CanModule::CCanAccess* m_busAccess;
};

int main(int argc, char * argv[])
{
  Log::initializeLogging(Log::TRC);
  Log::registerLoggingComponent("CanModule", Log::TRC);
  LOG(Log::INF) << __FUNCTION__ << "+";

  //CanLibLoaderPtr libLoader(CanModule::CanLibLoader::createInstance("MockUpCanImplementation"));
  CanLibLoaderPtr libLoader(CanModule::CanLibLoader::createInstance("sock"));
  CanModuleConnection connection("test", libLoader, CanModuleParams("sock:can1", "250000")); // mock impl: params ignored I guess

  LOG(Log::INF) << __FUNCTION__ << "+";
  return 0;
}
