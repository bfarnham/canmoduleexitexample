== build ==
Create a peer build directory (e.g. if this file is /<blah>/canmoduleexitexample/README.txt
then peer build directory might be /<blah>/canmoduleexitexample-build.

cd to canmoduleexitexample-build and invoke cmake to generate build
>> cmake ../canmoduleexitexample

in same directory invoke build
>> cmake --build .


== run ==
Just run ./theEXE after it builds (remember to point LD_LIBRARY_PATH at wherever
CanModule build spits out lib*can.so implementation libs)


== what are we looking at ?==
When I try to be a good C++ programmer and close the bus I created on exit
I get an error thrown.

Look at main.cpp; it's tiny and contains all the user code there is in this
demonstrator. See the comments in main()
